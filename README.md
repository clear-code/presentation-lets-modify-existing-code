# 既存のコードを変更する時の心得 ～ どこを見ればいいかの勘所 ～

## プレゼンテーションの表示

    cd presentation && rake

## 演習

https://app.vagrantup.com/okkez/boxes/workshop/versions/0.1.0 を使う。

作業用ディレクトリに以下の内容の Vagrantfile を用意して `vagrant up` を実行する。

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "okkez/workshop"
  config.vm.box_version = "0.1.0"
  config.vm.network "forwarded_port", guest: 3000, host: 3000
  config.vm.network "forwarded_port", guest: 80, host: 10080
end
```

### 環境

* OS: CentOS Linux release 7.5.1804 (Core)
* Ruby: ruby 2.5.1p57

Redmine はインストール済みで初期設定も完了している。
メールの受信用に RainLoop Webmail をインストールしてある。

以下のユーザーが Redmine とメールを使用できるように設定済みである。
メールのパスワードは各ユーザーのメールアドレスのローカルパートと同じである。

* admin@example.net
* user01@example.net
* user02@example.net
* user03@example.net
* user04@example.net
* user05@example.net

Redmine側のパスワードは `admin` ユーザーが `password` でその他のユーザーは `admin` ユーザーでログインしてから変更する。

### 使い方

ホスト側からウェブブラウザで `http://localhost:10080/mail` にアクセスするとウェブメールを使用できる。
Redmine を使用するときは、以下のようにしてサーバーを起動してからホスト側からウェブブラウザで `http://localhost:3000/` にアクセスする。

```
$ cd ~/ruby/redmine
$ bin/rails s -b 0.0.0.0
```

adminユーザーでログインし、作成済みのtestプロジェクトにチケットを作成する。
このときウォッチャーに全員追加しておくと、各ユーザーにメールが送信され、メールボックスが作成される。

各ユーザーのRedmine用のパスワードは、adminユーザーでログインした後、ユーザー管理画面から変更可能である。

### 課題

* http://www.redmine.org/issues/12416

Issue のコメントを編集したら、通知(diff付きメール)が飛ぶ機能をRedmine本体に組込む。

https://github.com/clear-code/redmine-plugin-journal-change-notifier


